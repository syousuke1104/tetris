﻿using NUnit.Framework;
using Tetris.Game;

namespace Tetris.Tests.Editor.Game {

    public class LevelDataTests {

        [TestCase(1, 5)]
        [TestCase(2, 15)]
        [TestCase(3, 30)]
        [TestCase(15, 600)]
        public void LinesRequiredForClearingTests(int level, int countOfLines) {
            var levelData = new LevelData(level);
            Assert.IsTrue(countOfLines == levelData.LinesRequiredForClearing);
        }
    }
}

﻿using Tetris.Game;
using UniRx.Toolkit;
using UnityEngine;
using UniRx;
using Cysharp.Threading.Tasks;
using System;

namespace Tetris.Graphics {

    public class LockDownEffectManager : MonoBehaviour {

        private class LockDownEffectPool : ObjectPool<ParticleSystem> {

            private readonly GameObject m_prefab;

            public LockDownEffectPool(GameObject prefab) => m_prefab = prefab;

            protected override ParticleSystem CreateInstance() =>
                Instantiate(m_prefab, m_prefab.transform.parent).GetComponent<ParticleSystem>();

        }

        [SerializeField]
        private GameObject m_effectPrefab;

        private void Start() {
            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.TetriminoTransformManager))
                .AddTo(this);
        }

        private void Initialize(TetriminoTransformer tetriminoTransformer) {

            var effectPool = new LockDownEffectPool(m_effectPrefab);

            tetriminoTransformer.OnLockDownAsObservable()
                .Subscribe(mino => {

                    var color = BlockSkinSettings.Instance.BlockColors[mino.BlockType];

                    foreach (var pos in mino.BlockPositions) {
                        var effect = effectPool.Rent();
                        effect.transform.localPosition = new Vector3(pos.x, pos.y);
                        
                        var main = effect.main;
                        main.startColor = color;

                        effect.Play();

                        UniTask.Void(async () => {
                            await UniTask.Delay(TimeSpan.FromSeconds(1.2));
                            effectPool.Return(effect);
                        });
                    }

                }).AddTo(this);
        }
    }
}

﻿using System.Linq;
using Tetris.Game;
using UnityEngine;

namespace Tetris.Graphics {

    public class TetriminoGraphics : MonoBehaviour {

        private Tetrimino m_tetrimino;

        public BlockType BlockType => m_tetrimino.BlockType;

        public void UpdateByTetrimino(Tetrimino mino) {
            m_tetrimino = mino;
            var blockGraphics = transform.GetComponentsInChildren<BlockGraphics>();
            
            foreach (var (localPos, i) in mino.Offsets.Select((p, i) => (p, i))) {
                blockGraphics[i].SetColor(mino.BlockType);
                blockGraphics[i].transform.localPosition = new Vector3(localPos.x, localPos.y);
            }
        }
    }
}

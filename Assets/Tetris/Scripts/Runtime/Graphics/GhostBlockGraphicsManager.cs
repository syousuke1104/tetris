﻿using System.Linq;
using Tetris.Game;
using UniRx;
using UnityEngine;

namespace Tetris.Graphics {

    public class GhostBlockGraphicsManager : MonoBehaviour {

        [SerializeField]
        private GameObject m_ghostBlockPrefab;

        private void Start() {
            m_ghostBlockPrefab.SetActive(false);

            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.GhostBlockManager))
                .AddTo(this);
        }

        private void Initialize(GhostBlockManager ghostBlockManager) {
            var ghostBlocks = Enumerable.Range(0, 4)
                .Select(_ => Instantiate(m_ghostBlockPrefab).GetComponent<GhostBlockGraphics>())
                .ToArray();

            ghostBlockManager.ShowGhostBlockRP
                .Subscribe(value => {
                    foreach (var ghostBlock in ghostBlocks) {
                        ghostBlock.Visible(value);
                    }
                }).AddTo(this);

            ghostBlockManager.OnUpdateGhostBlockPositionsAsObservable()
                .Subscribe(eventData => {
                    foreach (var (pos, i) in eventData.blockPositions.Select((pos, i) => (pos, i))) {
                        ghostBlocks[i].transform.position = new Vector3(pos.x + 0.5f, pos.y + 0.5f);
                        ghostBlocks[i].SetColor(eventData.blockType);
                    }
                }).AddTo(this);
        }
    }
}

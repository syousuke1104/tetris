﻿using Tetris.Game;
using TMPro;
using UniRx;
using UnityEngine;

namespace Tetris.Graphics.UI {

    public class GameStartCountdownTimer : MonoBehaviour {

        [SerializeField]
        private TextMeshProUGUI m_text;

        private Animator m_animator;

        private int m_animHash;

        private void Start() {
            m_animator = GetComponent<Animator>();
            m_animHash = Animator.StringToHash("Open");

            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.GameStartCountdownManager))
                .AddTo(this);
        }

        private void Initialize(GameStartCountdownManager countdownManager) {

            countdownManager.OnCountChangedAsObservable()
                .Where(count => count > 0)
                .Subscribe(UpdateText)
                .AddTo(this);

            m_text.text = string.Empty;
        }

        private void UpdateText(int count) {
            m_text.text = count.ToString();
            m_animator.Play(m_animHash, 0, 0f);
        }
    }
}

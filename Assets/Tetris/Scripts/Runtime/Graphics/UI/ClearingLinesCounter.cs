﻿using Tetris.Game;
using TMPro;
using UniRx;
using UnityEngine;

namespace Tetris.Graphics.UI {

    public class ClearingLinesCounter : MonoBehaviour {

        [SerializeField]
        private TextMeshProUGUI m_counterText;

        private void Start() {
            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.LineClearManager))
                .AddTo(this);
        }

        private void Initialize(LineClearManager lineClearManager) {
            lineClearManager.ClearingLinesCountRP
                .Subscribe(UpdateText)
                .AddTo(this);
        }

        private void UpdateText(int count) {
            m_counterText.text = count.ToString();
        }
    }
}

﻿using Cysharp.Threading.Tasks;
using System;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

namespace Tetris.Graphics.UI {

    public class BuildDateText : MonoBehaviour {

        private void Start() {
            InitializeAsync().Forget();
        }

        private async UniTask InitializeAsync() {

            var text = GetComponent<TextMeshProUGUI>();

            text.text = "Build date: ";

            var (success, date) = await TryGetBuildDateAsync();

            text.text += success ? date.ToLocalTime().ToString("G") : "Failed to get the build date.";

        }

        private async UniTask<(bool, DateTime)> TryGetBuildDateAsync() {

            if (string.IsNullOrWhiteSpace(Application.absoluteURL)) {
                Debug.LogWarning("Application.absoluteURL is empty.");
                return (false, default);
            }

            var url = new Uri(Path.Combine(Application.absoluteURL, "builddate"));

            using var request = UnityWebRequest.Get(url);
            request.timeout = 30;

            try {
                await request.SendWebRequest().WithCancellation(this.GetCancellationTokenOnDestroy());
            } catch {
                Debug.LogWarning("exception occurred during web request.");
            }

            if (request.result == UnityWebRequest.Result.Success) {
                var dateData = BitConverter.ToInt64(request.downloadHandler.data);
                var date = DateTime.FromBinary(dateData);
                return (true, date);
            } else {
                return (false, default);
            }
        }
    }
}

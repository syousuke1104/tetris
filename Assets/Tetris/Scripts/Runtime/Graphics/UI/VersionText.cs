﻿using TMPro;
using UnityEngine;

namespace Tetris.Graphics.UI {

    public class VersionText : MonoBehaviour {

        [SerializeField]
        private bool m_isUnityVersion;

        private void Start() {
            var text = GetComponent<TextMeshProUGUI>();

            if (m_isUnityVersion) {
                text.text = "Unity Version: " + Application.unityVersion;
            } else {
                text.text = "Version: " + Application.version;
            }
        }
    }
}

﻿using Tetris.Game;
using UniRx;
using UnityEngine;
using TMPro;

namespace Tetris.Graphics.UI {

    public class LevelCounter : MonoBehaviour {

        [SerializeField]
        private TextMeshProUGUI m_levelText;

        private void Start() {
            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.GameLevelManager))
                .AddTo(this);
        }

        private void Initialize(GameLevelManager gameLevelManager) {
            gameLevelManager.OnLevelChangedAsObservable()
                .Subscribe(UpdateText)
                .AddTo(this);
        }

        private void UpdateText(LevelData levelData) {
            m_levelText.text = levelData.Level.ToString();
        }
    }
}

﻿using Tetris.Game;
using UnityEngine;

namespace Tetris.Graphics {

    public class BlockGraphics : MonoBehaviour {

        private SpriteRenderer m_spriteRenderer;

        private SpriteRenderer SpriteRenderer => m_spriteRenderer ? m_spriteRenderer : (m_spriteRenderer = GetComponent<SpriteRenderer>());

        public void SetColor(BlockType type) {
            SpriteRenderer.color = BlockSkinSettings.Instance.BlockColors[type];
        }
    }
}
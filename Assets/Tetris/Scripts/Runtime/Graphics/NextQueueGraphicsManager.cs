﻿using System.Collections.Generic;
using System.Linq;
using Tetris.Game;
using UnityEngine;
using UniRx;

namespace Tetris.Graphics {

    public class NextQueueGraphicsManager : MonoBehaviour {

        [SerializeField]
        private GameObject m_nextTetriminoPrefab;

        private void Start() {
            m_nextTetriminoPrefab.SetActive(false);

            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.TetriminoGenerator))
                .AddTo(this);
        }

        private void Initialize(TetriminoGenerator tetriminoGenerator) {

            var nextTetriminoGraphicsQ = new Queue<TetriminoGraphics>(7);
            var objectPool = new Queue<TetriminoGraphics>(7);

            TetriminoGraphics GetFromPool() {
                return objectPool.Any() ? objectPool.Dequeue() : CreateNextTetriminoGraphics();
            }

            void ReturnToPool(TetriminoGraphics obj) => objectPool.Enqueue(obj);

            foreach (var (blockType, index) in tetriminoGenerator.NextBlockTypes.Select((b, i) => (b, i))) {
                var g = GetFromPool();

                nextTetriminoGraphicsQ.Enqueue(g);

                var mino = tetriminoGenerator.GetNewTetrimino(blockType);
                g.UpdateByTetrimino(mino);

                if (index < 7) {
                    g.transform.localPosition = CalcLocalPosition(blockType, index);
                    g.gameObject.SetActive(true);
                }
            }

            tetriminoGenerator.OnEnqueueNextAsObservable()
                .Subscribe(blockType => {
                    var g = GetFromPool();

                    nextTetriminoGraphicsQ.Enqueue(g);

                    var mino = tetriminoGenerator.GetNewTetrimino(blockType);
                    g.UpdateByTetrimino(mino);
                }).AddTo(this);

            tetriminoGenerator.OnDequeueNextAsObservable()
                .Subscribe(_ => {
                    {
                        var g = nextTetriminoGraphicsQ.Dequeue();
                        g.gameObject.SetActive(false);
                        ReturnToPool(g);
                    }

                    foreach (var (g, i) in nextTetriminoGraphicsQ.Take(7).Select((g, i) => (g, i))) {
                        if (!g.gameObject.activeSelf) {
                            g.gameObject.SetActive(true);
                        }
                        g.transform.localPosition = CalcLocalPosition(g.BlockType, i);
                    }
                }).AddTo(this);
        }

        private Vector3 CalcLocalPosition(BlockType blockType, int index) {
            var localPos = new Vector3(0f, 15.5f - index * 2.5f);

            if (index == 0) {
                localPos.y = 17.5f;
            }

            if (blockType == BlockType.Block_O) {
                localPos.x -= 0.5f;
            } else if (blockType == BlockType.Block_I) {
                localPos.x += 0.5f;
                localPos.y += 0.5f;
            }

            return localPos;
        }

        private TetriminoGraphics CreateNextTetriminoGraphics() {
            var obj = Instantiate(m_nextTetriminoPrefab, transform);
            return obj.GetComponent<TetriminoGraphics>();
        }
    }
}

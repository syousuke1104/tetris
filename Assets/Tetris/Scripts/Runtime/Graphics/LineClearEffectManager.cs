﻿using Cysharp.Threading.Tasks;
using System;
using System.Linq;
using Tetris.Game;
using UniRx;
using UniRx.Toolkit;
using UnityEngine;

namespace Tetris.Graphics {

    public class LineClearEffectManager : MonoBehaviour {

        private class DeleteLineEffectPool : ObjectPool<ParticleSystem> {

            private readonly GameObject m_prefab;

            public DeleteLineEffectPool(GameObject prefab) => m_prefab = prefab;

            protected override ParticleSystem CreateInstance() =>
                Instantiate(m_prefab, m_prefab.transform.parent).GetComponent<ParticleSystem>();

        }

        [SerializeField]
        private GameObject m_effectPrefab;

        private void Start() {
            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.LineClearManager))
                .AddTo(this);
        }

        private void Initialize(LineClearManager lineClearManager) {

            var deleteLineEffectPool = new DeleteLineEffectPool(m_effectPrefab);

            lineClearManager.OnDeleteLinesAsObservable()
                .Subscribe(lines => {

                    foreach (var (isDelted, index) in lines.Select((l, i) => (l, i))) {

                        if (!isDelted) continue;

                        var effect = deleteLineEffectPool.Rent();
                        effect.transform.localPosition = new Vector3(5f, index, -0.1f);
                        effect.Play();

                        UniTask.Void(async () => {
                            await UniTask.Delay(TimeSpan.FromSeconds(1));
                            deleteLineEffectPool.Return(effect);
                        });
                    }
                }).AddTo(this);
        }
    }
}

﻿using Tetris.Game;
using UnityEngine;
using UniRx;

namespace Tetris.Graphics {

    public class HoldBlockGraphics : MonoBehaviour {

        [SerializeField]
        private TetriminoGraphics m_tetriminoG;

        private void Start() {
            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.FieldManager))
                .AddTo(this);
        }

        private void Initialize(FieldManager fieldManager) {
            fieldManager.OnHoldingCurrentMinoAsObservable()
                .Subscribe(_ => {
                    if (fieldManager.TryGetHoldingTetrimino(out var mino)) {
                        Show(mino);
                    } else {
                        Hide();
                    }
                }).AddTo(this);

            Hide();
        }

        private void Show(Tetrimino mino) {
            m_tetriminoG.UpdateByTetrimino(mino);

            var localPos = Vector3.zero;

            localPos.y = -0.5f;

            if (mino.BlockType == BlockType.Block_O) {
                localPos.x = -0.5f;
            } else if (mino.BlockType == BlockType.Block_I) {
                localPos.x = 0.5f;
                localPos.y = 0f;
            }

            m_tetriminoG.transform.localPosition = localPos;
            m_tetriminoG.gameObject.SetActive(true);
        }

        private void Hide() {
            m_tetriminoG.gameObject.SetActive(false);
        }
    }
}

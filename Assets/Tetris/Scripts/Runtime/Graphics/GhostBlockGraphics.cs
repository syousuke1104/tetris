﻿namespace Tetris.Graphics {

    public class GhostBlockGraphics : BlockGraphics {

        public void Visible(bool isVisible) {
            gameObject.SetActive(isVisible);
        }
    }
}

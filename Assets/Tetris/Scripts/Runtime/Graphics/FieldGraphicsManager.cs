﻿using Tetris.Game;
using UniRx;
using UnityEngine;

namespace Tetris.Graphics {

    public class FieldGraphicsManager : MonoBehaviour {

        [SerializeField]
        private GameObject m_blockPrefab;

        private void Start() {
            m_blockPrefab.SetActive(false);

            GameSceneController.Instance.OnInitGameManagerAsObservable()
                .Subscribe(gm => Initialize(gm.FieldManager))
                .AddTo(this);
        }

        private void Initialize(FieldManager fieldManager) {

            var fs = fieldManager.FieldSize;
            var blockGraphicsMap = new BlockGraphics[fs.y, fs.x];

            var (w, h) = (fs.x, fs.y);

            for (int row = 0; row < h; row++) {
                for (int col = 0; col < w; col++) {
                    var obj = Instantiate(m_blockPrefab, new Vector3(col + 0.5f, row + 0.5f), Quaternion.identity, transform);
                    var blockGraphics = obj.GetComponent<BlockGraphics>();

                    blockGraphics.SetColor(fieldManager.GetData(row, col));
                    obj.SetActive(true);

                    blockGraphicsMap[row, col] = blockGraphics;
                }
            }

            fieldManager.OnUpdateFieldDataAsObservable()
                .Subscribe(data => blockGraphicsMap[data.Row, data.Colomn].SetColor(data.BlockType))
                .AddTo(this);

            var mask = GetComponent<SpriteMask>();
            var tex = new Texture2D(1000, 2050);
            mask.sprite = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero);
        }
    }
}

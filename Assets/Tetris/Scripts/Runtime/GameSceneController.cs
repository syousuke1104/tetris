﻿using System;
using Tetris.Game;
using UniRx;
using UnityEngine;

namespace Tetris {

    public class GameSceneController : MonoBehaviour {

        private AsyncSubject<GameManager> m_onInitGameManagerSubject;

        public IObservable<GameManager> OnInitGameManagerAsObservable() => m_onInitGameManagerSubject;

        public static GameSceneController Instance { get; private set; }

        private void Awake() {
            Instance = this;
            m_onInitGameManagerSubject = new AsyncSubject<GameManager>();
        }

        private void Start() {
            var gameManager = new GameManager();

            gameManager.AddTo(this);

            gameManager.Initialize();
            m_onInitGameManagerSubject.OnNext(gameManager);
            m_onInitGameManagerSubject.OnCompleted();

            gameManager.GameStart();
        }

        private void OnDestroy() {
            Instance = null;
            m_onInitGameManagerSubject.Dispose();
        }
    }
}

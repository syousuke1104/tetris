﻿using System.Collections.Generic;
using Tetris.Game;
using UnityEngine;

namespace Tetris {

    public class BlockSkinSettings {

        public static readonly BlockSkinSettings Instance;

        static BlockSkinSettings() => Instance = new BlockSkinSettings();

        private readonly Dictionary<BlockType, Color> m_blockColors;

        public IReadOnlyDictionary<BlockType, Color> BlockColors => m_blockColors;

        private BlockSkinSettings() {

            static Color HtmlColor(string htmlColor) => ColorUtility.TryParseHtmlString(htmlColor, out var c) ? c : Color.black;

            m_blockColors = new Dictionary<BlockType, Color>(EqualityComparer<BlockType>.Default) {
                { BlockType.Empty, Color.clear },
                { BlockType.Block_I,  HtmlColor("#33d6ff") },
                { BlockType.Block_J,  HtmlColor("#3384ff") },
                { BlockType.Block_L,  HtmlColor("#ff8433") },
                { BlockType.Block_O,  HtmlColor("#ffd433") },
                { BlockType.Block_S,  HtmlColor("#84ff33") },
                { BlockType.Block_T,  HtmlColor("#d633ff") },
                { BlockType.Block_Z,  HtmlColor("#ff3333") },
                { BlockType.Wall,  HtmlColor("#b2b2b2") },
            };
        }
    }
}

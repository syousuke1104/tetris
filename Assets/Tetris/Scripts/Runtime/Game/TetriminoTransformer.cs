﻿using System;
using UniRx;
using UnityEngine;
using static Tetris.Game.Tetrimino;

namespace Tetris.Game {

    public class TetriminoTransformer : IDisposable {

        private readonly FieldManager m_fieldManager;

        private readonly TetriminoRotationCheckerTypeI m_rotationManagerTypeI;

        private readonly TetriminoRotationCheckerTypeO m_rotationManagerTypeO;

        private readonly TetriminoRotationCheckerTypeTJLSZ m_rotationManagerTypeTJLSZ;

        private readonly Subject<Tetrimino> m_onTranslateTetriminoSubject;

        private readonly Subject<Tetrimino> m_onHardDroppedSubject;

        private readonly IObservable<Tetrimino> m_onLockDownAsObservable;

        public IObservable<Tetrimino> OnTranslateTetriminoAsObservable() => m_onTranslateTetriminoSubject;

        public IObservable<Tetrimino> OnLockDownAsObservable() => m_onLockDownAsObservable;

        public IObservable<Tetrimino> OnHardDroppedAsObservable() => m_onHardDroppedSubject;

        private bool m_hardDropped;

        private readonly CompositeDisposable m_disposables;

        public TetriminoTransformer(FieldManager fieldManager) {
            m_fieldManager = fieldManager;

            m_disposables = new CompositeDisposable() {
                (m_onTranslateTetriminoSubject = new Subject<Tetrimino>()),
                (m_onHardDroppedSubject = new Subject<Tetrimino>()),
            };
            
            m_rotationManagerTypeI = new TetriminoRotationCheckerTypeI(fieldManager);
            m_rotationManagerTypeTJLSZ = new TetriminoRotationCheckerTypeTJLSZ(fieldManager);
            m_rotationManagerTypeO = new TetriminoRotationCheckerTypeO();

            m_onLockDownAsObservable = OnTranslateTetriminoAsObservable()
                .Throttle(TimeSpan.FromSeconds(0.5))
                .Where(_ => !m_fieldManager.CanMoveDownCurrentTetrimino() && !m_hardDropped)
                .Merge(OnHardDroppedAsObservable())
                .Share();


            fieldManager.OnGenerateTetriminoAsObservable()
                .Subscribe(_ => m_hardDropped = false)
                .AddTo(m_disposables);
        }

        private bool CommonTransformableCheck(out Tetrimino mino) {

            if (!m_fieldManager.TryGetCurrentTetrimino(out mino)) {
                Debug.LogWarning("current tetrimino is null");
                return false;
            }

            if (mino.LifetimeToTransform <= 0) {
                Debug.LogWarning("not enogh tetrimino's lifetime.");
                return false;
            }

            if (m_hardDropped) {
                Debug.LogWarning("hard dropped! wait a moment.");
                return false;
            }

            return true;
        }

        public bool CanHardDropCurrentTetrimino(out Tetrimino mino, out Vector2Int movement) {
            movement = Vector2Int.zero;

            if (!CommonTransformableCheck(out mino)) return false;

            movement = Vector2Int.down;

            if (!m_fieldManager.CanPlaceTetrimino(mino, movement, mino.Rotation)) {
                Debug.LogWarning("current tetrimino cannot move");
                movement = Vector2Int.zero;
                return false;
            }

            while (m_fieldManager.CanPlaceTetrimino(mino, movement + Vector2Int.down, mino.Rotation)) {
                movement += Vector2Int.down;
            }

            return true;
        }

        public void HardDropCurrentTetrimino() {
            if (CanHardDropCurrentTetrimino(out var mino, out var movement)) {
                m_hardDropped = true;
                UpdateFieldData(mino, movement, mino.Rotation);
                m_onHardDroppedSubject.OnNext(mino);
            }
        }

        public void MoveCurrentTetrimino(Vector2Int movement) {

            if (!CommonTransformableCheck(out var mino)) return;

            if (m_fieldManager.CanPlaceTetrimino(mino, movement, mino.Rotation)) {
                UpdateFieldData(mino, movement, mino.Rotation);
            } else {
                Debug.LogWarning("current tetrimino cannot move");
            }
        }

        public void RotateCurrentTetrimino(bool turnRight) {

            if (!CommonTransformableCheck(out var mino)) return;

            var rotChecker = mino.BlockType switch {
                BlockType.Block_I => m_rotationManagerTypeI,
                BlockType.Block_O => m_rotationManagerTypeO,
                BlockType.Empty | BlockType.Wall => throw new System.NotImplementedException(),
                _ => (TetriminoRotationChecker)m_rotationManagerTypeTJLSZ,
            };

            if (rotChecker.CanRotateTetrimino(mino, turnRight, out var movement)) {
                UpdateFieldData(mino, movement, mino.GetNextRotations(turnRight));
            } else {
                Debug.LogWarning("current tetrimino cannot rotate");
            }
        }

        private void UpdateFieldData(Tetrimino tetrimino, Vector2Int movement, Rotations nextRotation) {
            var lastMinHeight = tetrimino.ArrivedMinHeight;

            m_fieldManager.SetEmptyFromPositions(tetrimino.BlockPositions);
            tetrimino.Move(movement);
            tetrimino.Rotation = nextRotation;
            m_fieldManager.SetDataFormTetrimino(tetrimino);

            if (tetrimino.Position.y < lastMinHeight) {
                tetrimino.ResetLifetimeToTransform();
            } else {
                if (!m_fieldManager.CanPlaceTetrimino(tetrimino, Vector2Int.down, tetrimino.Rotation)) {
                    tetrimino.DecrementLifetimeToTransform();
                }
            }

            m_onTranslateTetriminoSubject.OnNext(tetrimino);
        }

        public void Dispose() {
            m_disposables.Dispose();
        }
    }
}

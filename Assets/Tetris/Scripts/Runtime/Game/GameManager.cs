﻿using Cysharp.Threading.Tasks;
using System;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Tetris.Game {

    public class GameManager : IDisposable {

        public readonly GameStartCountdownManager GameStartCountdownManager;

        public readonly FieldManager FieldManager;

        public readonly TetriminoTransformer TetriminoTransformManager;

        private readonly TetriminoAutoTransformer m_tetriminoAutoTransformer;

        public readonly LineClearManager LineClearManager;

        private readonly GameInputManager m_gameInputManager;

        private readonly CompositeDisposable m_disposables;

        public readonly TetriminoGenerator TetriminoGenerator;

        public readonly GhostBlockManager GhostBlockManager;

        public readonly GameLevelManager GameLevelManager;

        private readonly ReactiveProperty<GameStates> m_state;

        public IReadOnlyReactiveProperty<GameStates> StateRP => m_state;

        public bool GameStateIsRunning => StateRP.Value == GameStates.Running;

        public IObservable<Unit> OnGameStartedAsObservable() => StateRP.Where(s => s == GameStates.Running).AsUnitObservable();

        public GameManager() {
            m_disposables = new CompositeDisposable() {
                (FieldManager = new FieldManager(new Vector2Int(10, 21))),
                (TetriminoTransformManager = new TetriminoTransformer(FieldManager)),
                (m_tetriminoAutoTransformer = new TetriminoAutoTransformer()),
                (LineClearManager = new LineClearManager(FieldManager)),
                (TetriminoGenerator = new TetriminoGenerator()),
                (GhostBlockManager = new GhostBlockManager(FieldManager, TetriminoTransformManager)),
                (GameStartCountdownManager = new GameStartCountdownManager()),
                (m_state = new ReactiveProperty<GameStates>(GameStates.None)),
                (GameLevelManager = new GameLevelManager(LineClearManager))
            };

            m_gameInputManager = new GameInputManager();
        }

        public void Initialize() {
            FieldManager.Initialize(TetriminoTransformManager);

            m_gameInputManager.TetriminoMoveAsObservable()
                .Where(_ => GameStateIsRunning)
                .Subscribe(movement => TetriminoTransformManager.MoveCurrentTetrimino(movement))
                .AddTo(m_disposables);

            m_gameInputManager.TetriminoRotateRightAsObservable().Select(_ => true)
                .Merge(m_gameInputManager.TetriminoRotateLeftAsObservable().Select(_ => false))
                .Where(_ => GameStateIsRunning)
                .Subscribe(turnRight => TetriminoTransformManager.RotateCurrentTetrimino(turnRight))
                .AddTo(m_disposables);

            m_gameInputManager.TetriminoHardDropAsObservable()
                .Where(_ => GameStateIsRunning)
                .Subscribe(_ => TetriminoTransformManager.HardDropCurrentTetrimino())
                .AddTo(m_disposables);

            //m_gameInputManager.PlaceDebugBlockAsObservable()
            //    .Subscribe(p => FieldManager.SetData(p.y, p.x, BlockType.Block_I))
            //    .AddTo(m_disposables);

            //m_gameInputManager.PlaceDeubgEmptyAsObservable()
            //    .Subscribe(p => FieldManager.SetData(p.y, p.x, BlockType.Empty))
            //    .AddTo(m_disposables);

            m_gameInputManager.TetriminoHoldAsObservable()
                .Where(_ => GameStateIsRunning)
                .Subscribe(_ => FieldManager.HoldCurrentTetrimino())
                .AddTo(m_disposables);

            TetriminoTransformManager.OnLockDownAsObservable()
                .Subscribe(_ => UniTask.Void(async () => {
                    var deletedLines = LineClearManager.DeleteLines();
                    if (deletedLines.Where(x => x).Any()) {
                        await UniTask.Delay(TimeSpan.FromSeconds(1));
                    }
                    LineClearManager.MoveDownLines(deletedLines);

                    var success = FieldManager.GenerateNewTetrimino(TetriminoGenerator.GetNewRandomTetrimino());
                    if (!success) {
                        Debug.Log("lose");
                        SetState(GameStates.Result);
                    }
                }));

            FieldManager.OnHoldingCurrentMinoAsObservable()
                .Subscribe(_ => {
                    if (!FieldManager.TryGetCurrentTetrimino(out var _)) {
                        FieldManager.GenerateNewTetrimino(TetriminoGenerator.GetNewRandomTetrimino());
                    }
                });

            GameLevelManager.OnLevelChangedAsObservable()
                .Subscribe(levelData => {
                    m_tetriminoAutoTransformer.Interval = levelData.FallSpeed;
                    m_tetriminoAutoTransformer.StartAutoMove(FieldManager, TetriminoTransformManager);
                });

            OnGameStartedAsObservable().Subscribe(_ => OnGameStarted());

            StateRP.Subscribe(state => Debug.Log($"GameManager state: {state}"));
        }

        private void OnGameStarted() {
            FieldManager.GenerateNewTetrimino(TetriminoGenerator.GetNewRandomTetrimino());
            m_tetriminoAutoTransformer.StartAutoMove(FieldManager, TetriminoTransformManager);
        }

        private void SetState(GameStates state) => m_state.Value = state;

        public void GameStart() {
            UniTask.Void(async () => {
                await GameStartCountdownManager.StartCountdownAsync();
                SetState(GameStates.Running);
            });
        }

        public void GenerateDebugBlock(BlockType blockType) {
            FieldManager.GenerateNewTetrimino(TetriminoGenerator.GetNewTetrimino(blockType));
        }

        public void Dispose() {
            m_disposables.Dispose();
        }
    }
}

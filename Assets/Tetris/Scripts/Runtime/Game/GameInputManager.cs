﻿using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Tetris.Game {

    public class GameInputManager {

        private readonly GameObject m_trigger;

        public IObservable<Unit> TetriminoRotateRightAsObservable() =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.D));

        public IObservable<Unit> TetriminoRotateLeftAsObservable() =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.A));

        public IObservable<Unit> TetriminoHoldAsObservable() =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.W));

        public IObservable<Unit> TetriminoHardDropAsObservable() =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(KeyCode.UpArrow));

        public IObservable<Vector2Int> TetriminoMoveAsObservable() =>
            Observable.Merge(
                TetriminoMoveAsObservable(KeyCode.DownArrow, Vector2Int.down),
                TetriminoMoveAutoAsObservable(KeyCode.DownArrow, Vector2Int.down),
                TetriminoMoveAsObservable(KeyCode.RightArrow, Vector2Int.right),
                TetriminoMoveAutoAsObservable(KeyCode.RightArrow, Vector2Int.right),
                TetriminoMoveAsObservable(KeyCode.LeftArrow, Vector2Int.left),
                TetriminoMoveAutoAsObservable(KeyCode.LeftArrow, Vector2Int.left));

        public IObservable<Vector2Int> TetriminoMoveAsObservable(KeyCode keyCode, Vector2Int movement) =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(keyCode))
                .Select(_ => movement);

        public IObservable<Vector2Int> TetriminoMoveAutoAsObservable(KeyCode keyCode, Vector2Int movement) =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetKeyDown(keyCode))
                .Throttle(TimeSpan.FromSeconds(0.3))
                .SelectMany(_ => m_trigger.UpdateAsObservable())
                .TakeUntil(m_trigger.UpdateAsObservable().Where(_ => Input.GetKeyUp(keyCode)))
                .RepeatUntilDestroy(m_trigger)
                .Where(_ => Input.GetKey(keyCode))
                .ThrottleFirst(TimeSpan.FromMilliseconds(50))
                .Select(_ => movement);

        public IObservable<Vector2Int> PlaceDebugBlockAsObservable() =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetMouseButtonDown(0))
                .Select(_ => Camera.main.ScreenToWorldPoint(Input.mousePosition))
                .Select(p => new Vector2Int(Mathf.FloorToInt(p.x), Mathf.FloorToInt(p.y)));

        public IObservable<Vector2Int> PlaceDeubgEmptyAsObservable() =>
            m_trigger.UpdateAsObservable()
                .Where(_ => Input.GetMouseButtonDown(1))
                .Select(_ => Camera.main.ScreenToWorldPoint(Input.mousePosition))
                .Select(p => new Vector2Int(Mathf.FloorToInt(p.x), Mathf.FloorToInt(p.y)));

        public GameInputManager() {
            m_trigger = new GameObject(nameof(GameInputManager));
        }
    }
}

﻿namespace Tetris.Game {

    public enum BlockType : byte {
        Empty,
        Block_I,
        Block_O,
        Block_S,
        Block_Z,
        Block_J,
        Block_L,
        Block_T,
        Wall,
    }
}

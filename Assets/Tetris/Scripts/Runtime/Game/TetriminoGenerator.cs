﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace Tetris.Game {

    public class TetriminoGenerator : IDisposable {

        private readonly Queue<BlockType> m_bag;

        private readonly Subject<BlockType> m_onEnqueueNextSubject;

        private readonly Subject<BlockType> m_onDequeueNextSubject;

        public IEnumerable<BlockType> NextBlockTypes => m_bag;

        public IObservable<BlockType> OnEnqueueNextAsObservable() => m_onEnqueueNextSubject;

        public IObservable<BlockType> OnDequeueNextAsObservable() => m_onDequeueNextSubject;

        public TetriminoGenerator() {
            m_bag = new Queue<BlockType>();
            m_onEnqueueNextSubject = new Subject<BlockType>();
            m_onDequeueNextSubject = new Subject<BlockType>();
            RefillBag();
        }

        public Tetrimino GetNewRandomTetrimino() {
            if (m_bag.Count <= 7) {
                RefillBag();
            }

            var value = m_bag.Dequeue();
            
            m_onDequeueNextSubject.OnNext(value);

            return GetNewTetrimino(value);
        }

        public Tetrimino GetNewTetrimino(BlockType blockType) {
            return blockType switch {
                BlockType.Block_I => new TetriminoTypeI(),
                BlockType.Block_J => new TetriminoTypeJ(),
                BlockType.Block_L => new TetriminoTypeL(),
                BlockType.Block_O => new TetriminoTypeO(),
                BlockType.Block_S => new TetriminoTypeS(),
                BlockType.Block_T => new TetriminoTypeT(),
                BlockType.Block_Z => new TetriminoTypeZ(),
                _ => throw new NotImplementedException(),
            };
        }

        private void RefillBag() {
            foreach (var id in Enumerable.Range(1, 7).OrderBy(_ => UnityEngine.Random.value)) {
                var value = (BlockType)id;
                m_bag.Enqueue(value);
                m_onEnqueueNextSubject.OnNext(value);
            }
        }

        public void Dispose() {
            m_onEnqueueNextSubject.Dispose();
            m_onDequeueNextSubject.Dispose();
        }
    }
}

﻿using System;
using UniRx;

namespace Tetris.Game {

    public class TetriminoAutoTransformer : IDisposable {

        public float Interval { get; set; } = 1f;

        private IDisposable m_autoMoveHandler;

        public void StartAutoMove(FieldManager fieldManager, TetriminoTransformer transformer) {
            m_autoMoveHandler?.Dispose();
            
            m_autoMoveHandler = Observable.Timer(TimeSpan.FromSeconds(Interval), TimeSpan.FromSeconds(Interval))
                .TakeUntil(fieldManager.OnGenerateTetriminoAsObservable())
                .Repeat()
                .Subscribe(_ => transformer.MoveCurrentTetrimino(UnityEngine.Vector2Int.down));
        }

        public void Dispose() {
            m_autoMoveHandler?.Dispose();
        }
    }
}

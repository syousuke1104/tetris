﻿using System;
using UnityEngine;
using static Tetris.Game.Tetrimino;

namespace Tetris.Game {

    public class TetriminoRotationCheckerTypeI : TetriminoRotationChecker {

        private readonly FieldManager m_fieldManager;

        public TetriminoRotationCheckerTypeI(FieldManager fieldManager) {
            m_fieldManager = fieldManager;
        }

        public override bool CanRotateTetrimino(Tetrimino mino, bool turnRight, out Vector2Int movement) {

            static Vector2Int Move(int x, int y) => new Vector2Int(x, y);

            movement = Vector2Int.zero;
            var fm = m_fieldManager;
            var nextRot = mino.GetNextRotations(turnRight);

            if (fm.CanPlaceTetrimino(mino, Vector2Int.zero, nextRot)) {
                return true;
            } else {
                return mino.Rotation switch {
                    Rotations.R_0 =>
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? -2 : -1, 0), nextRot) ||
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? 1 : 2, 0), nextRot) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-2, -1), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-1, 2), nextRot)) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(1, 2), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(2, -1), nextRot)),
                    Rotations.R_90 =>
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? -1 : 2, 0), nextRot) ||
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? 2 : -1, 0), nextRot) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-1, 2), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(2, 1), nextRot)) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(2, -1), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-1, -2), nextRot)),
                    Rotations.R_180 =>
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? 2 : 1, 0), nextRot) ||
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? -1 : -2, 0), nextRot) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(2, 1), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(1, -2), nextRot)) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-1, -2), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-2, 1), nextRot)),
                    Rotations.R_270 =>
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? 1 : -2, 0), nextRot) ||
                        fm.CanPlaceTetrimino(mino, movement = Move(turnRight ? -2 : 1, 0), nextRot) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(1, -2), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-2, -1), nextRot)) ||
                        (turnRight && fm.CanPlaceTetrimino(mino, movement = Move(-2, 1), nextRot)) ||
                        (!turnRight && fm.CanPlaceTetrimino(mino, movement = Move(1, 2), nextRot)),
                    _ => throw new NotImplementedException(),
                };
            }
        }
    }
}

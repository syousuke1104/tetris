﻿using System;
using System.Linq;
using UnityEngine;

namespace Tetris.Game {

    public class TetriminoTypeZ : Tetrimino {

        private readonly Vector2Int[] m_offsets_R0;

        private readonly Vector2Int[] m_offsets_R90;

        private readonly Vector2Int[] m_offsets_R180;

        private readonly Vector2Int[] m_offsets_R270;

        public TetriminoTypeZ() : base(BlockType.Block_Z) {
            m_offsets_R0 = new[] { V(0, 0), V(0, 1), V(-1, 1), V(1, 0) };
            m_offsets_R90 = new[] { V(0, 0), V(0, -1), V(1, 0), V(1, 1) };
            m_offsets_R180 = m_offsets_R0.Select(p => p + Vector2Int.down).ToArray();
            m_offsets_R270 = m_offsets_R90.Select(p => p + Vector2Int.left).ToArray();
        }

        public override Vector2Int[] GetRotatedOffsets(Rotations rotation) {
            return rotation switch {
                Rotations.R_0 => m_offsets_R0,
                Rotations.R_90 => m_offsets_R90,
                Rotations.R_180 => m_offsets_R180,
                Rotations.R_270 => m_offsets_R270,
                _ => throw new NotImplementedException(),
            };
        }
    }
}

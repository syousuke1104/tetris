﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tetris.Game {

    public abstract class Tetrimino {

        public enum Rotations {
            R_0,
            R_90,
            R_180,
            R_270
        }

        public BlockType BlockType { get; }

        public Rotations Rotation { get; set; }

        public Vector2Int Position { get; private set; }

        public Vector2Int[] Offsets => GetRotatedOffsets(Rotation);

        public int LifetimeToTransform { get; private set; }

        public int ArrivedMinHeight { get; private set; }

        public Tetrimino(BlockType blockType) {
            BlockType = blockType;
            ResetLifetimeToTransform();
        }

        public abstract Vector2Int[] GetRotatedOffsets(Rotations rotation);

        public void ResetLifetimeToTransform() => LifetimeToTransform = 15;

        public void DecrementLifetimeToTransform() => LifetimeToTransform--;

        public void SetPosition(Vector2Int position) {
            ArrivedMinHeight = position.y;
            Position = position;
        }

        public void Move(Vector2Int movement) {
            Position += movement;
            ArrivedMinHeight = Mathf.Min(ArrivedMinHeight, Position.y);
        }

        protected Vector2Int V(int x, int y) => new Vector2Int(x, y);

        public IEnumerable<Vector2Int> BlockPositions => Offsets.Select(o => Position + o);

        public IEnumerable<Vector2Int> GetRotatedBlockPositions(Rotations rotation) =>
            GetRotatedOffsets(rotation).Select(o => Position + o);

        public bool IsInside(Vector2Int pos) {
            return BlockPositions.Any(org => org == pos);
        }

        public bool IsOutside(Vector2Int pos) => !IsInside(pos);

        public Rotations GetNextRotations(bool turnRight) {
            return Rotation switch {
                Rotations.R_0 => turnRight ? Rotations.R_90 : Rotations.R_270,
                Rotations.R_90 => turnRight ? Rotations.R_180 : Rotations.R_0,
                Rotations.R_180 => turnRight ? Rotations.R_270 : Rotations.R_90,
                Rotations.R_270 => turnRight ? Rotations.R_0 : Rotations.R_180,
                _ => throw new System.NotImplementedException(),
            };
        }

    }
}

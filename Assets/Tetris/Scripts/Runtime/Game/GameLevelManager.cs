﻿using System;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Tetris.Game {

    public class GameLevelManager : IDisposable {

        public LevelData CurrentLevelData { get; private set; }

        public IObservable<LevelData> OnLevelChangedAsObservable() => m_onLevelChangedSubject;

        private readonly BehaviorSubject<LevelData> m_onLevelChangedSubject;

        private readonly CompositeDisposable m_disposables;

        public GameLevelManager(LineClearManager lineClearManager) {
            CurrentLevelData = new LevelData(1);
            m_disposables = new CompositeDisposable() {
                (m_onLevelChangedSubject = new BehaviorSubject<LevelData>(CurrentLevelData))
            };

            static LevelData GetNextLevelData(int currentLines, LevelData levelData) {
                var next = new LevelData(levelData.Level + 1);

                if (currentLines >= next.LinesRequiredForClearing) {
                    return GetNextLevelData(currentLines, next);
                } else {
                    return next;
                }
            }

            lineClearManager.ClearingLinesCountRP
                .Where(count => count >= CurrentLevelData.LinesRequiredForClearing)
                .Subscribe(count => {
                    var next = GetNextLevelData(count, CurrentLevelData);
                    CurrentLevelData = next;
                    m_onLevelChangedSubject.OnNext(next);
                }).AddTo(m_disposables);

            OnLevelChangedAsObservable()
                .Subscribe(data => Debug.Log($"Level: {data.Level}, FallSpeed: {data.FallSpeed * 1000}ms, Lines: {data.LinesRequiredForClearing}"))
                .AddTo(m_disposables);
        }

        public void Dispose() {
            m_disposables.Dispose();
        }
    }
}

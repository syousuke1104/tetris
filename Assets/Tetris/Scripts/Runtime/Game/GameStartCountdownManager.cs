﻿using Cysharp.Threading.Tasks;
using System;
using UniRx;

namespace Tetris.Game {

    public class GameStartCountdownManager : IDisposable {

        private readonly Subject<int> m_countChangedSubject;

        private bool m_isRunning;

        public IObservable<int> OnCountChangedAsObservable() => m_countChangedSubject;

        public GameStartCountdownManager() {
            m_countChangedSubject = new Subject<int>();
        }

        public async UniTask StartCountdownAsync() {
            if (m_isRunning) {
                UnityEngine.Debug.LogWarning("countdown is running");
                return;
            }

            m_isRunning = true;

            await CountdownAsync();

            m_isRunning = false;
        }

        private async UniTask CountdownAsync() {
            var count = 3;
            m_countChangedSubject.OnNext(count);

            while (count > 0) {
                await UniTask.Delay(TimeSpan.FromSeconds(1));
                count--;
                m_countChangedSubject.OnNext(count);
            }
        }

        public void Dispose() {
            m_countChangedSubject.Dispose();
        }
    }
}

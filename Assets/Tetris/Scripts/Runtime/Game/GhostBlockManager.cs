﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Tetris.Game {

    public class GhostBlockManager : IDisposable {

        private readonly BoolReactiveProperty m_showGhostBlockRP;

        private readonly Subject<(BlockType, IEnumerable<Vector2Int>)> m_onUpdateGhostBlockPositionsSubject;

        public IReadOnlyReactiveProperty<bool> ShowGhostBlockRP => m_showGhostBlockRP;

        public IObservable<(BlockType blockType, IEnumerable<Vector2Int> blockPositions)> OnUpdateGhostBlockPositionsAsObservable() => m_onUpdateGhostBlockPositionsSubject;

        public GhostBlockManager(FieldManager fieldManager, TetriminoTransformer tetriminoTransformer) {

            m_showGhostBlockRP = new BoolReactiveProperty();
            m_onUpdateGhostBlockPositionsSubject = new Subject<(BlockType, IEnumerable<Vector2Int>)>();

            fieldManager.OnUpdateFieldDataAsObservable()
                .ThrottleFrame(1)
                .Subscribe(_ => {
                    var canHardDrop = tetriminoTransformer.CanHardDropCurrentTetrimino(out var mino, out var movement);

                    if (canHardDrop) {
                        m_onUpdateGhostBlockPositionsSubject.OnNext((mino.BlockType, mino.BlockPositions.Select(p => p + movement)));
                    }

                    m_showGhostBlockRP.Value = canHardDrop;
                });
        }

        public void Dispose() {
            m_showGhostBlockRP.Dispose();
            m_onUpdateGhostBlockPositionsSubject.Dispose();
        }
    }
}

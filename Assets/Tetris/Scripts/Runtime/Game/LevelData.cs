﻿using System.Linq;
using UnityEngine;

namespace Tetris.Game {

    public class LevelData {

        public readonly int Level;

        public readonly int LinesRequiredForClearing;

        public readonly float FallSpeed;

        public LevelData(int level) {
            Level = level;
            LinesRequiredForClearing = Enumerable.Range(1, level)
                .Select(l => l * 5)
                .Sum();
            FallSpeed = Mathf.Pow(0.8f - (level - 1) * 0.007f, level - 1);
        }
    }
}

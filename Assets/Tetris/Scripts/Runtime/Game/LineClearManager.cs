﻿using System;
using System.Linq;
using UniRx;

namespace Tetris.Game {

    public class LineClearManager : IDisposable {

        private readonly FieldManager m_fieldManager;

        private readonly Subject<bool[]> m_onDeleteLinesSubject;

        private readonly ReadOnlyReactiveProperty<int> m_clearingLinesCountRP;

        public IReadOnlyReactiveProperty<int> ClearingLinesCountRP => m_clearingLinesCountRP;

        public IObservable<bool[]> OnDeleteLinesAsObservable() => m_onDeleteLinesSubject;

        public LineClearManager(FieldManager fieldManager) {
            m_fieldManager = fieldManager;
            m_onDeleteLinesSubject = new Subject<bool[]>();
            m_clearingLinesCountRP = OnDeleteLinesAsObservable()
                .Select(lines => lines.Select(b => b ? 1 : 0).Sum())
                .Scan((acc, c) => acc + c)
                .ToReadOnlyReactiveProperty(0);
        }

        public bool[] DeleteLines() {
            var fm = m_fieldManager;
            var deletedLines = new bool[fm.FieldSize.y];

            foreach (var row in Enumerable.Range(0, fm.FieldSize.y)) {
                var line = fm.GetRowData(row);
                
                if (line.Where(b => b == BlockType.Empty).Any()) {
                    continue; // not filled
                }

                deletedLines[row] = true;

                fm.SetRowData(row, BlockType.Empty);
            }

            m_onDeleteLinesSubject.OnNext(deletedLines);

            return deletedLines;
        }

        public void MoveDownLines(bool[] deletedLines) {

            int count = 0;
            var fm = m_fieldManager;

            foreach (var row in Enumerable.Range(0, fm.FieldSize.y - 1)) {

                if (deletedLines[row]) {
                    count++;
                    continue;
                }

                var line = fm.GetRowData(row);

                foreach (var data in line.Select((blockType, index) => (blockType, index))) {
                    fm.SetData(row - count, data.index, data.blockType);
                }
            }

            foreach (var row in Enumerable.Range(fm.FieldSize.y - count - 1, count)) {
                fm.SetRowData(row, BlockType.Empty);
            }
        }

        public void Dispose() {
            m_onDeleteLinesSubject.Dispose();
            m_clearingLinesCountRP.Dispose();
        }
    }
}

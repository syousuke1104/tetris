﻿namespace Tetris.Game {

    public enum GameStates {
        None,
        Ready,
        Running,
        Pause,
        Result
    }
}

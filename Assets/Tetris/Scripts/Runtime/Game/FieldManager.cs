﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using static Tetris.Game.Tetrimino;

namespace Tetris.Game {

    public class FieldManager : IDisposable {

        public struct FieldUpdateEventData {
            public int Row;
            public int Colomn;
            public BlockType BlockType;
            public FieldUpdateEventData(int row, int col, BlockType blockType) {
                Row = row;
                Colomn = col;
                BlockType = blockType;
            }
        }

        public Vector2Int FieldSize { get; }

        private readonly BlockType[,] m_fieldData;

        private readonly Subject<FieldUpdateEventData> m_onUpdateFieldDataSubject;

        private readonly Subject<Tetrimino> m_onGenerateTetriminoSubject;

        private readonly Subject<Unit> m_onHoldingCurrentMino;

        private readonly CompositeDisposable m_disposables;

        private bool m_didHoldOrSwapMino;

        private Tetrimino m_currentMino;

        private Tetrimino m_holdingMino;

        public IObservable<FieldUpdateEventData> OnUpdateFieldDataAsObservable() => m_onUpdateFieldDataSubject;

        public IObservable<Tetrimino> OnGenerateTetriminoAsObservable() => m_onGenerateTetriminoSubject;

        public IObservable<Unit> OnHoldingCurrentMinoAsObservable() => m_onHoldingCurrentMino;

        public FieldManager(Vector2Int fieldSize) {
            FieldSize = fieldSize;
            m_fieldData = new BlockType[fieldSize.y, fieldSize.x];
            m_disposables = new CompositeDisposable() {
                (m_onUpdateFieldDataSubject = new Subject<FieldUpdateEventData>()),
                (m_onGenerateTetriminoSubject = new Subject<Tetrimino>()),
                (m_onHoldingCurrentMino = new Subject<Unit>()),
            };
        }

        public void Initialize(TetriminoTransformer tetriminoTransformer) {
            var (w, h) = (FieldSize.x, FieldSize.y);

            for (int row = 0; row < h; row++) {
                for (int col = 0; col < w; col++) {
                    m_fieldData[row, col] = BlockType.Empty;
                }
            }

            m_currentMino = null;

            OnGenerateTetriminoAsObservable()
                .Subscribe(_ => m_didHoldOrSwapMino = false)
                .AddTo(m_disposables);

            tetriminoTransformer.OnLockDownAsObservable()
                .Subscribe(_ => m_currentMino = null)
                .AddTo(m_disposables);
        }

        public BlockType GetData(int row, int col) => m_fieldData[row, col];

        public IEnumerable<BlockType> GetRowData(int row) {
            foreach (var col in Enumerable.Range(0, FieldSize.x)) {
                yield return m_fieldData[row, col];
            }
        }

        public void SetRowData(int row, BlockType blockType) {
            foreach (var col in Enumerable.Range(0, FieldSize.x)) {
                SetData(row, col, blockType);
            }
        }

        public bool CanMoveDownCurrentTetrimino() {
            if (TryGetCurrentTetrimino(out var mino)) {
                return CanPlaceTetrimino(mino, Vector2Int.down, mino.Rotation);
            }
            return false;
        }

        public bool GenerateNewTetrimino(Tetrimino mino) {

            var success = GenerateNewTetriminoWithoutEventCall(mino);

            if (success) {
                m_onGenerateTetriminoSubject.OnNext(mino);
            }

            return success;
        }

        private bool GenerateNewTetriminoWithoutEventCall(Tetrimino mino) {

            var offset_x = mino.BlockType == BlockType.Block_I ? 0 : -1;
            mino.SetPosition(new Vector2Int(FieldSize.x / 2 + offset_x, FieldSize.y - 2));

            if (IsEmpty(mino.BlockPositions)) {
                m_currentMino = mino;
                SetDataFormTetrimino(mino);
                return true;
            }

            return false;
        }

        public bool TryGetCurrentTetrimino(out Tetrimino tetrimino) {
            tetrimino = m_currentMino;
            return !(tetrimino is null);
        }

        public bool TryGetHoldingTetrimino(out Tetrimino tetrimino) {
            tetrimino = m_holdingMino;
            return !(tetrimino is null);
        }

        public void HoldCurrentTetrimino() {
            if (m_didHoldOrSwapMino) {
                Debug.LogWarning("already holding tetrimino");
                return;
            }
            m_didHoldOrSwapMino = true;

            var tmp = m_holdingMino;
            m_holdingMino = m_currentMino;
            m_currentMino = tmp;

            if (TryGetHoldingTetrimino(out var mino)) {
                SetEmptyFromPositions(mino.BlockPositions);
                mino.Rotation = Rotations.R_0;
            }

            if (TryGetCurrentTetrimino(out mino)) {
                GenerateNewTetriminoWithoutEventCall(mino);
            }

            m_onHoldingCurrentMino.OnNext(Unit.Default);
        }

        public bool CanPlaceTetrimino(Tetrimino tetrimino, Vector2Int slideOffset, Rotations rotation) {

            if (!IsEmpty(tetrimino.GetRotatedBlockPositions(rotation)
                    .Select(p => p + slideOffset)
                    .Where(p => tetrimino.IsOutside(p)))) {
                //Debug.LogWarning("not empty");
                return false;
            }

            return true;
        }

        public bool IsEmpty(IEnumerable<Vector2Int> positions) {
            return !positions.Where(p => !IsEmpty(p)).Any();
        }

        public bool IsEmpty(Vector2Int pos) => IsEmpty(pos.y, pos.x);

        public bool IsEmpty(int row, int col) {
            if (row < 0 || row >= FieldSize.y) {
                Debug.LogWarning("out of field height");
                return false;
            }
            
            if (col < 0 || col >= FieldSize.x) {
                Debug.LogWarning("out of field width");
                return false;
            }

            return m_fieldData[row, col] == BlockType.Empty;
        }

        public void SetEmptyFromPositions(IEnumerable<Vector2Int> positions) {
            SetDataFormPositions(positions, BlockType.Empty);
        }

        public void SetDataFormTetrimino(Tetrimino mino) {
            SetDataFormPositions(mino.BlockPositions, mino.BlockType);
        }

        public void SetDataFormPositions(IEnumerable<Vector2Int> positions, BlockType blockType) {
            foreach (var pos in positions) {
                SetData(pos.y, pos.x, blockType);
            }
        }

        public void SetData(int row, int col, BlockType blockType) {
            m_fieldData[row, col] = blockType;
            m_onUpdateFieldDataSubject.OnNext(new FieldUpdateEventData(row, col, blockType));
        }

        public void Dispose() {
            m_disposables.Dispose();
        }
    }
}

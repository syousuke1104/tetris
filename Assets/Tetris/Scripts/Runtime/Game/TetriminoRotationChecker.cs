﻿using UnityEngine;

namespace Tetris.Game {

    public abstract class TetriminoRotationChecker {

        public abstract bool CanRotateTetrimino(Tetrimino tetrimino, bool turnRight, out Vector2Int movement);

    }
}

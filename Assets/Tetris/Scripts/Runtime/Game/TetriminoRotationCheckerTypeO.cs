﻿using UnityEngine;

namespace Tetris.Game {

    public class TetriminoRotationCheckerTypeO : TetriminoRotationChecker {

        public override bool CanRotateTetrimino(Tetrimino tetrimino, bool turnRight, out Vector2Int movement) {
            // Do noting
            movement = Vector2Int.zero;
            return true;
        }
    }
}

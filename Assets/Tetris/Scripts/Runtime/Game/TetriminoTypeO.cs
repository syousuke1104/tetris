﻿using UnityEngine;

namespace Tetris.Game {

    public class TetriminoTypeO : Tetrimino {

        private readonly Vector2Int[] m_offsets_R0;

        public TetriminoTypeO() : base(BlockType.Block_O) {
            m_offsets_R0 = new[] { V(0, 0), V(0, 1), V(1, 0), V(1, 1) };
        }

        public override Vector2Int[] GetRotatedOffsets(Rotations rotation) {
            return m_offsets_R0;
        }
    }
}

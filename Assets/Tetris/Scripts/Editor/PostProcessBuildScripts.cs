using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;

namespace Tetris.Editor {

    public class PostProcessBuildScripts {

        [PostProcessBuild]
        public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
            
            var now = System.DateTime.UtcNow;

            using var stream = File.Create(Path.Combine(pathToBuiltProject, "builddate"));
            using var bw = new BinaryWriter(stream);
            
            bw.Write(now.ToBinary());
        }
    }
}
